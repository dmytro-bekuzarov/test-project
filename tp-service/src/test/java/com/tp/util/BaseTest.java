package com.tp.util;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

@RunWith(EasyMockRunner.class)
public abstract class BaseTest extends EasyMockSupport {

    @Before
    public void setUp() throws Exception {
        resetAll();
    }

    @After
    public void tearDown() throws Exception {
        verifyAll();
    }

}
