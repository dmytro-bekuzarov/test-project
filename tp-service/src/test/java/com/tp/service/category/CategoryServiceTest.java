package com.tp.service.category;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static java.util.Collections.singletonList;

import com.tp.repository.category.CategoryRepository;
import com.tp.repository.category.UserCategoryRepository;
import com.tp.user.Category;
import com.tp.util.BaseTest;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class CategoryServiceTest extends BaseTest {

    private static final String USER_ID = "userId";

    @TestSubject
    private CategoryService categoryService = new CategoryService();

    @Mock
    private CategoryRepository categoryRepository;
    @Mock
    private UserCategoryRepository userCategoryRepository;

    private Category category;

    @Before
    public void setUp() throws Exception {
        category = new Category();
        category.setId("id");
        category.setName("name");
    }

    @Test
    public void getAll() {
        List<Category> expectedCategories = singletonList(category);
        expect(categoryRepository.findAll()).andReturn(expectedCategories);
        replayAll();

        List<Category> categories = categoryService.getAll();
        assertEquals(expectedCategories, categories);
    }

    @Test
    public void getIds() {
        List<String> expectedCategoryIds = singletonList(category.getId());
        expect(categoryRepository.findIds(expectedCategoryIds)).andReturn(expectedCategoryIds);
        replayAll();

        List<String> categoryIds = categoryService.getIds(expectedCategoryIds);
        assertEquals(expectedCategoryIds, categoryIds);
    }

    @Test
    public void add() {
        categoryRepository.insert(category);
        expectLastCall();
        replayAll();

        category.setId(null);
        categoryService.add(category);
        assertNotNull(category.getId());
    }

    @Test
    public void update() {
        expect(categoryRepository.update(category)).andReturn(false);
        replayAll();

        boolean updated = categoryService.update(category);
        assertFalse(updated);
    }

    @Test
    public void delete() {
        userCategoryRepository.deleteByCategoryId(category.getId());
        expectLastCall();
        expect(categoryRepository.deleteById(category.getId())).andReturn(false);
        replayAll();

        boolean deleted = categoryService.delete(category.getId());
        assertFalse(deleted);
    }

    @Test
    public void getCategoryIdsByUserId() {
        List<String> expectedCategoryIds = singletonList(category.getId());
        expect(userCategoryRepository.findCategoryIdsByUserId(USER_ID)).andReturn(expectedCategoryIds);
        replayAll();

        List<String> categoryIds = categoryService.getCategoryIdsByUserId(USER_ID);
        assertEquals(expectedCategoryIds, categoryIds);
    }

    @Test
    public void addUserCategories() {
        List<String> categoryIds = singletonList(category.getId());
        userCategoryRepository.insert(categoryIds, USER_ID);
        expectLastCall();
        replayAll();

        categoryService.addUserCategories(categoryIds, USER_ID);
    }

    @Test
    public void replaceUserCategories() {
        List<String> categoryIds = singletonList(category.getId());
        userCategoryRepository.deleteByUserId(USER_ID);
        expectLastCall();
        userCategoryRepository.insert(categoryIds, USER_ID);
        expectLastCall();
        replayAll();

        categoryService.replaceUserCategories(categoryIds, USER_ID);
    }

    @Test
    public void deleteUserCategoriesByUserId() {
        userCategoryRepository.deleteByUserId(USER_ID);
        expectLastCall();
        replayAll();

        categoryService.deleteUserCategoriesByUserId(USER_ID);
    }

}