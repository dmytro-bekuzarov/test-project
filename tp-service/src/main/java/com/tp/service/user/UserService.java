package com.tp.service.user;

import com.tp.repository.category.UserCategoryRepository;
import com.tp.repository.user.UserRepository;
import com.tp.transaction.Transaction;
import com.tp.user.User;
import com.tp.user.UserDetails;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * User service.
 *
 * @author Dmytro Bekuzarov
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserCategoryRepository userCategoryRepository;

    /**
     * Gets all user details.
     *
     * @return the list of user details
     */
    public List<UserDetails> getAllUserDetails() {
        return userRepository.findAllUserDetails();
    }

    /**
     * Gets user details by id.
     *
     * @param userId the user id
     * @return the list of user details
     */
    public UserDetails getUserDetailsById(String userId) {
        return userRepository.findUserDetailsById(userId);
    }

    /**
     * Add new user.
     *
     * @param user the user
     */
    @Transaction
    public void add(User user) {
        user.setId(UUID.randomUUID().toString());
        user.setPassword(DigestUtils.sha256Hex(user.getPassword()));
        user.setCreateTime(new Date());
        userRepository.insert(user);
    }

    /**
     * Update existing user.
     *
     * @param user the user
     * @return {@code true} if user was updated, {@code false} otherwise
     */
    public boolean update(User user) {
        user.setPassword(DigestUtils.sha256Hex(user.getPassword()));
        return userRepository.update(user);
    }

    /**
     * Delete user by id.
     *
     * @param userId the user id
     * @return {@code true} if user was deleted, {@code false} otherwise
     */
    @Transaction
    public boolean delete(String userId) {
        userCategoryRepository.deleteByUserId(userId);
        return userRepository.deleteById(userId);
    }

    /**
     * Check if user exists.
     *
     * @param userId the user id
     * @return {@code true} if user exists, {@code false} otherwise
     */
    public boolean exists(String userId) {
        return userRepository.exists(userId);
    }
}
