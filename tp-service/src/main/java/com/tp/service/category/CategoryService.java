package com.tp.service.category;


import com.tp.repository.category.CategoryRepository;
import com.tp.repository.category.UserCategoryRepository;
import com.tp.transaction.Transaction;
import com.tp.user.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Category service.
 *
 * @author Dmytro Bekuzarov
 */
@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private UserCategoryRepository userCategoryRepository;

    /**
     * Get all categories.
     *
     * @return the list of categories
     */
    public List<Category> getAll() {
        return categoryRepository.findAll();
    }

    /**
     * Get ids of specified categories.
     *
     * @param categoryIds the category ids
     * @return the ids
     */
    public List<String> getIds(Collection<String> categoryIds) {
        return categoryRepository.findIds(categoryIds);
    }

    /**
     * Add new category.
     *
     * @param category the category
     */
    public void add(Category category) {
        category.setId(UUID.randomUUID().toString());
        categoryRepository.insert(category);
    }

    /**
     * Update existing category.
     *
     * @param category the category
     * @return {@code true} if category was updated, {@code false} otherwise
     */
    public boolean update(Category category) {
        return categoryRepository.update(category);
    }

    /**
     * Delete category by id.
     *
     * @param categoryId the category id
     * @return {@code true} if category was deleted, {@code false} otherwise
     */
    @Transaction
    public boolean delete(String categoryId) {
        userCategoryRepository.deleteByCategoryId(categoryId);
        return categoryRepository.deleteById(categoryId);
    }

    /**
     * Gets category ids by user id.
     *
     * @param userId the user id
     * @return the category ids
     */
    public List<String> getCategoryIdsByUserId(String userId) {
        return userCategoryRepository.findCategoryIdsByUserId(userId);
    }

    /**
     * Add user categories.
     *
     * @param categories the categories
     * @param userId     the user id
     */
    public void addUserCategories(Collection<String> categories, String userId) {
        userCategoryRepository.insert(categories, userId);
    }

    /**
     * Replace user categories.
     *
     * @param categories the categories
     * @param userId     the user id
     */
    @Transaction
    public void replaceUserCategories(Collection<String> categories, String userId) {
        userCategoryRepository.deleteByUserId(userId);
        userCategoryRepository.insert(categories, userId);
    }

    /**
     * Delete user categories by user id.
     *
     * @param userId the user id
     */
    public void deleteUserCategoriesByUserId(String userId) {
        userCategoryRepository.deleteByUserId(userId);
    }
}
