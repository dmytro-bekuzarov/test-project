package com.tp.transaction;

import com.tp.repository.ConnectionHolder;
import com.tp.repository.ConnectionManager;
import com.tp.repository.exception.DAOException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Handles transactions.
 *
 * @author Dmytro Bekuzarov
 */
@Aspect
@Component
public class TransactionHandler {

    @Autowired
    private ConnectionManager connectionManager;

    /**
     * Executes service method in transaction in case {@link Transaction} annotation is present on this method.
     *
     * @param joinPoint the join point
     * @return the return object
     * @throws Throwable the throwable
     */
    @Around("execution(* com.tp.service..*.*(..))")
    public Object execute(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Transaction annotation = method.getAnnotation(Transaction.class);
        if (annotation == null) {
            return invokeWithoutTransaction(joinPoint);
        }
        return invokeWithTransaction(joinPoint);
    }

    private Object invokeWithoutTransaction(ProceedingJoinPoint joinPoint) throws Throwable {
        Connection connection = connectionManager.getConnection();
        ConnectionHolder.set(connection);
        try {
            connection.setAutoCommit(true);
            return joinPoint.proceed(joinPoint.getArgs());
        } catch (Exception e) {
            throw new DAOException("Cannot invoke", e);
        } finally {
            closeConnection(connection);
            ConnectionHolder.remove();
        }
    }

    private Object invokeWithTransaction(ProceedingJoinPoint joinPoint) throws Throwable {
        Connection connection = connectionManager.getConnection();
        ConnectionHolder.set(connection);
        try {
            Object result;
            connection.setAutoCommit(false);
            try {
                result = joinPoint.proceed(joinPoint.getArgs());
            } catch (Exception e) {
                throw new DAOException("Cannot invoke", e);
            }
            connection.commit();
            return result;
        } catch (Exception e) {
            rollback(connection);
            throw new DAOException("Cannot commit transaction", e);
        } finally {
            closeConnection(connection);
            ConnectionHolder.remove();
        }
    }

    private void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new DAOException("Cannot close connection", e);
        }
    }

    private void rollback(Connection connection) {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new DAOException("Cannot rollback", e);
        }
    }


}
