package com.tp.rest.converter.user;

import com.tp.rest.converter.AbstractConverter;
import com.tp.rest.converter.category.CategoryConverter;
import com.tp.rest.domain.user.UserDetailsDto;
import com.tp.user.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserDetailsConverter extends AbstractConverter<UserDetails, UserDetailsDto> {

    @Autowired
    private UserConverter userConverter;
    @Autowired
    private CategoryConverter categoryConverter;

    @Override
    protected UserDetailsDto init() {
        return new UserDetailsDto();
    }

    @Override
    protected void populate(UserDetailsDto userDetailsDto, UserDetails userDetails) {
        userConverter.populate(userDetailsDto, userDetails.getUser());
        userDetailsDto.setCreateTime(userDetails.getUser().getCreateTime());
        userDetailsDto.setCategories(categoryConverter.convert(userDetails.getCategories()));
    }
}