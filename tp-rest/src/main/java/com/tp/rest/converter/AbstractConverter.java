package com.tp.rest.converter;

import java.util.ArrayList;
import java.util.List;

/**
 * Converts objects of source type {@code S} to target type {@code T}.
 *
 * @param <S> the source type
 * @param <T> the target type
 * @author Dmytro Bekuzarov
 */
public abstract class AbstractConverter<S, T> {

    /**
     * Convert source to target.
     *
     * @param source the source
     * @return the target
     */
    public T convert(S source) {
        T target = init();
        populate(target, source);
        return target;
    }

    /**
     * Convert list.
     *
     * @param sourceList the source list
     * @return the target list
     */
    public List<T> convert(List<S> sourceList) {
        List<T> targetList = new ArrayList<>(sourceList.size());
        for (S source : sourceList) {
            targetList.add(convert(source));
        }
        return targetList;
    }

    protected abstract T init();

    protected abstract void populate(T target, S source);

}
