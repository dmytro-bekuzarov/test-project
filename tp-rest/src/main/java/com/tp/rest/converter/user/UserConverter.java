package com.tp.rest.converter.user;

import com.tp.rest.converter.AbstractConverter;
import com.tp.rest.domain.user.SaveUserRequest;
import com.tp.rest.domain.user.UserDto;
import com.tp.user.User;
import org.springframework.stereotype.Component;

@Component
public class UserConverter extends AbstractConverter<User, UserDto> {

    @Override
    protected UserDto init() {
        return new UserDto();
    }

    @Override
    protected void populate(UserDto userDto, User user) {
        userDto.setUserId(user.getId());
        userDto.setUsername(user.getUsername());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
    }

    public User toUser(SaveUserRequest request) {
        User user = new User();
        user.setUsername(request.getUsername());
        user.setEmail(request.getEmail());
        user.setPassword(request.getPassword());
        return user;
    }
}
