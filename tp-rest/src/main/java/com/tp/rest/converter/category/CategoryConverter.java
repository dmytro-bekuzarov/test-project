package com.tp.rest.converter.category;

import com.tp.rest.converter.AbstractConverter;
import com.tp.rest.domain.category.SaveCategoryRequest;
import com.tp.rest.domain.user.CategoryDto;
import com.tp.user.Category;
import org.springframework.stereotype.Component;

@Component
public class CategoryConverter extends AbstractConverter<Category, CategoryDto> {

    @Override
    protected CategoryDto init() {
        return new CategoryDto();
    }

    @Override
    protected void populate(CategoryDto categoryDto, Category category) {
        categoryDto.setId(category.getId());
        categoryDto.setName(category.getName());
    }

    public Category toCategory(SaveCategoryRequest request) {
        Category category = new Category();
        category.setName(request.getName());
        return category;
    }


}
