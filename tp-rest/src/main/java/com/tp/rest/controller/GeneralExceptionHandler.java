package com.tp.rest.controller;

import com.tp.rest.domain.response.ErrorResponse;
import com.tp.rest.exception.RestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
@ResponseBody
public class GeneralExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralExceptionHandler.class);

    @Autowired
    private MessageSource messageSource;


    @ExceptionHandler(RestException.class)
    public ErrorResponse handle(RestException ex, HttpServletResponse response) {
        response.setStatus(ex.getStatus().value());
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setStatus(ex.getStatus().name());
        errorResponse.setMessage(getLocalizedMessage(ex.getMessage(), ex.getArgs()));
        return errorResponse;
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handle(HttpRequestMethodNotSupportedException ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setStatus(HttpStatus.NOT_FOUND.name());
        errorResponse.setMessage(getLocalizedMessage("http.method.not.supported", ex.getMethod()));
        return errorResponse;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorResponse handle(Exception ex) {
        LOGGER.error("Internal server error occurred", ex);
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.name());
        errorResponse.setMessage(getLocalizedMessage("internal.server.error"));
        return errorResponse;
    }

    private String getLocalizedMessage(String messageText, Object... args) {
        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest req = sra.getRequest();
        try {
            return messageSource.getMessage(messageText, args, req.getLocale());
        } catch (Exception ex) {
            LOGGER.warn("Cannot get message", ex);
            return messageText;
        }
    }
}
