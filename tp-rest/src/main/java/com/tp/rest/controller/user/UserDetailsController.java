package com.tp.rest.controller.user;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import com.tp.rest.converter.user.UserDetailsConverter;
import com.tp.rest.domain.user.UserDetailsDto;
import com.tp.rest.validator.UserValidator;
import com.tp.service.user.UserService;
import com.tp.user.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * User details controller.
 *
 * @author Dmytro Bekuzarov
 */
@RestController
public class UserDetailsController {

    @Autowired
    private UserValidator userValidator;
    @Autowired
    private UserService userService;
    @Autowired
    private UserDetailsConverter userDetailsConverter;

    /**
     * Gets all users details.
     *
     * @return the list of user details
     */
    @RequestMapping(value = "/users/details", method = GET)
    public List<UserDetailsDto> getAllUsers() {
        List<UserDetails> userDetailsList = userService.getAllUserDetails();
        return userDetailsConverter.convert(userDetailsList);
    }

    /**
     * Gets user details by id.
     *
     * @param userId the user id
     * @return the user details
     */
    @RequestMapping(value = "/users/{userId}/details", method = GET)
    public UserDetailsDto getUserById(@PathVariable String userId) {
        userValidator.checkUserExists(userId);
        UserDetails userDetails = userService.getUserDetailsById(userId);
        return userDetailsConverter.convert(userDetails);
    }


}
