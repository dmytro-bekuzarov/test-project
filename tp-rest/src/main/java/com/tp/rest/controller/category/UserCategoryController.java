package com.tp.rest.controller.category;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import com.tp.rest.domain.category.CategoryIds;
import com.tp.rest.validator.CategoryValidator;
import com.tp.rest.validator.UserValidator;
import com.tp.service.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * User category controller.
 *
 * @author Dmytro Bekuzarov
 */
@RestController
public class UserCategoryController {

    @Autowired
    private UserValidator userValidator;
    @Autowired
    private CategoryValidator categoryValidator;
    @Autowired
    private CategoryService categoryService;

    /**
     * Add user categories.
     *
     * @param userId      the user id
     * @param categoryIds the category ids
     */
    @RequestMapping(value = "/users/{userId}/categories", method = POST)
    public void addUserCategories(@PathVariable String userId, @RequestBody CategoryIds categoryIds) {
        userValidator.checkUserExists(userId);

        Set<String> categories = new LinkedHashSet<>(categoryIds.getCategories());
        categoryValidator.validateCategories(categories);

        categories.removeAll(categoryService.getCategoryIdsByUserId(userId));

        categoryService.addUserCategories(categories, userId);
    }

    /**
     * Replace user categories.
     *
     * @param userId      the user id
     * @param categoryIds the category ids
     */
    @RequestMapping(value = "/users/{userId}/categories", method = PUT)
    public void replaceUserCategories(@PathVariable String userId, @RequestBody CategoryIds categoryIds) {
        userValidator.checkUserExists(userId);

        Set<String> categories = new LinkedHashSet<>(categoryIds.getCategories());
        categoryValidator.validateCategories(categories);

        categoryService.replaceUserCategories(categories, userId);
    }

    /**
     * Delete user categories.
     *
     * @param userId the user id
     */
    @RequestMapping(value = "/users/{userId}/categories", method = DELETE)
    public void deleteUserCategories(@PathVariable String userId) {
        userValidator.checkUserExists(userId);

        categoryService.deleteUserCategoriesByUserId(userId);
    }

}
