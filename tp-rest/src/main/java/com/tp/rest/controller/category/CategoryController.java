package com.tp.rest.controller.category;


import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import com.tp.rest.converter.category.CategoryConverter;
import com.tp.rest.domain.category.SaveCategoryRequest;
import com.tp.rest.domain.user.CategoryDto;
import com.tp.rest.exception.NotFoundException;
import com.tp.service.category.CategoryService;
import com.tp.user.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Category rest controller.
 *
 * @author Dmytro Bekuzarov
 */
@RestController
public class CategoryController {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CategoryConverter categoryConverter;

    /**
     * Get all categories.
     *
     * @return list of categories
     */
    @RequestMapping(value = "/categories", method = GET)
    public List<CategoryDto> getAllCategories() {
        List<Category> categories = categoryService.getAll();
        return categoryConverter.convert(categories);
    }

    /**
     * Add new category.
     *
     * @param categoryRequest the category
     * @return added category
     */
    @RequestMapping(value = "/categories", method = POST)
    public CategoryDto addCategory(@RequestBody SaveCategoryRequest categoryRequest) {
        Category category = categoryConverter.toCategory(categoryRequest);
        categoryService.add(category);
        return categoryConverter.convert(category);
    }

    /**
     * Update category by id.
     *
     * @param categoryId      the category id
     * @param categoryRequest the category
     * @return updated category
     */
    @RequestMapping(value = "/categories/{categoryId}", method = PUT)
    public CategoryDto updateCategory(@PathVariable String categoryId,
                                      @RequestBody SaveCategoryRequest categoryRequest) {
        Category category = categoryConverter.toCategory(categoryRequest);
        category.setId(categoryId);
        boolean updated = categoryService.update(category);
        if (updated) {
            return categoryConverter.convert(category);
        } else {
            throw new NotFoundException("category.not.found", categoryId);
        }
    }

    /**
     * Delete category by id.
     *
     * @param categoryId the category id
     */
    @RequestMapping(value = "/categories/{categoryId}", method = DELETE)
    public void deleteCategory(@PathVariable String categoryId) {
        boolean deleted = categoryService.delete(categoryId);
        if (!deleted) {
            throw new NotFoundException("category.not.found", categoryId);
        }
    }

}
