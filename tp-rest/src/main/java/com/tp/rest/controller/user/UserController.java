package com.tp.rest.controller.user;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import com.tp.rest.converter.user.UserConverter;
import com.tp.rest.domain.user.SaveUserRequest;
import com.tp.rest.domain.user.UserDto;
import com.tp.rest.exception.NotFoundException;
import com.tp.service.user.UserService;
import com.tp.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * User controller.
 *
 * @author Dmytro Bekuzarov
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserConverter userConverter;

    /**
     * Add new user.
     *
     * @param userRequest the user request
     * @return added user
     */
    @RequestMapping(value = "/users", method = POST)
    public UserDto addUser(@RequestBody SaveUserRequest userRequest) {
        User user = userConverter.toUser(userRequest);
        userService.add(user);
        return userConverter.convert(user);
    }

    /**
     * Update existing user.
     *
     * @param userId      the user id
     * @param userRequest the user request
     * @return updated user
     */
    @RequestMapping(value = "/users/{userId}", method = PUT)
    public UserDto updateUser(@PathVariable String userId, @RequestBody SaveUserRequest userRequest) {
        User user = userConverter.toUser(userRequest);
        user.setId(userId);
        boolean updated = userService.update(user);
        if (updated) {
            return userConverter.convert(user);
        } else {
            throw new NotFoundException("user.not.found", userId);
        }
    }

    /**
     * Delete user by id.
     *
     * @param userId the user id
     */
    @RequestMapping(value = "/users/{userId}", method = DELETE)
    public void deleteUser(@PathVariable String userId) {
        boolean deleted = userService.delete(userId);
        if (!deleted) {
            throw new NotFoundException("user.not.found", userId);
        }
    }

}
