package com.tp.rest.validator;

import com.tp.rest.exception.NotFoundException;
import com.tp.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserValidator {

    @Autowired
    private UserService userService;

    public void checkUserExists(String userId) {
        if (!userService.exists(userId)) {
            throw new NotFoundException("user.not.found", userId);
        }
    }

}
