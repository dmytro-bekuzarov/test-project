package com.tp.rest.validator;

import static org.apache.commons.collections4.SetUtils.difference;

import com.tp.rest.exception.BadRequestException;
import com.tp.service.category.CategoryService;
import org.apache.commons.collections4.SetUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class CategoryValidator {

    @Autowired
    private CategoryService categoryService;

    public void validateCategories(Set<String> categoryIds) {
        List<String> foundIds = categoryService.getIds(categoryIds);
        SetUtils.SetView<String> difference = difference(categoryIds, new HashSet<>(foundIds));
        if (!difference.isEmpty()) {
            throw new BadRequestException("invalid.category.ids", difference.toSet());
        }
    }

}
