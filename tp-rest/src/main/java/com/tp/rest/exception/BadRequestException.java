package com.tp.rest.exception;


import org.springframework.http.HttpStatus;

public class BadRequestException extends RestException {

    public BadRequestException(String message, Object... args) {
        super(HttpStatus.BAD_REQUEST, message, args);
    }

}
