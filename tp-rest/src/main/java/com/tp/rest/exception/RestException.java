package com.tp.rest.exception;

import org.springframework.http.HttpStatus;

public class RestException extends RuntimeException {

    private HttpStatus status;
    private Object[] args;

    RestException(HttpStatus status, String message, Object[] args) {
        super(message);
        this.status = status;
        this.args = args;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public Object[] getArgs() {
        return args;
    }
}
