package com.tp.rest.converter.user;

import static org.junit.Assert.assertEquals;

import com.tp.rest.domain.user.SaveUserRequest;
import com.tp.rest.domain.user.UserDto;
import com.tp.user.User;
import org.junit.Test;

public class UserConverterTest {

    private UserConverter userConverter = new UserConverter();

    @Test
    public void toUser() {
        SaveUserRequest request = new SaveUserRequest();
        request.setEmail("email");
        request.setPassword("password");
        request.setUsername("username");
        User user = userConverter.toUser(request);
        assertEquals(request.getEmail(), user.getEmail());
        assertEquals(request.getPassword(), user.getPassword());
        assertEquals(request.getUsername(), user.getUsername());
    }

    @Test
    public void toDto() {
        User user = new User();
        user.setId("id");
        user.setEmail("email");
        user.setPassword("password");
        user.setUsername("username");
        UserDto userDto = userConverter.convert(user);
        assertEquals(user.getId(), userDto.getUserId());
        assertEquals(user.getEmail(), userDto.getEmail());
        assertEquals(user.getPassword(), userDto.getPassword());
        assertEquals(user.getUsername(), userDto.getUsername());
    }

}