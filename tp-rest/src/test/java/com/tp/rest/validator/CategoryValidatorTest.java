package com.tp.rest.validator;

import static org.easymock.EasyMock.expect;

import com.tp.rest.exception.BadRequestException;
import com.tp.service.category.CategoryService;
import com.tp.util.BaseTest;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class CategoryValidatorTest extends BaseTest {

    @TestSubject
    private CategoryValidator categoryValidator = new CategoryValidator();

    @Mock
    private CategoryService categoryService;

    @Test
    public void validateCategoriesIfAllValid() {
        Set<String> categories = new HashSet<>(Arrays.asList("id1", "id2"));
        expect(categoryService.getIds(categories)).andReturn(Arrays.asList("id2", "id1"));
        replayAll();

        categoryValidator.validateCategories(categories);
    }

    @Test(expected = BadRequestException.class)
    public void validateCategoriesIfSomeInvalid() {
        Set<String> categories = new HashSet<>(Arrays.asList("id1", "id2"));
        expect(categoryService.getIds(categories)).andReturn(Collections.singletonList("id2"));
        replayAll();

        categoryValidator.validateCategories(categories);
    }

    @Test(expected = BadRequestException.class)
    public void validateCategoriesIfAllInvalid() {
        Set<String> categories = new HashSet<>(Arrays.asList("id1", "id2"));
        expect(categoryService.getIds(categories)).andReturn(Collections.<String>emptyList());
        replayAll();

        categoryValidator.validateCategories(categories);
    }


}