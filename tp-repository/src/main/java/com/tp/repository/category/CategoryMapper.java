package com.tp.repository.category;

import com.tp.repository.Mapper;
import com.tp.user.Category;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CategoryMapper implements Mapper<Category> {

    @Override
    public Category map(ResultSet resultSet) throws SQLException {
        Category category = new Category();
        category.setId(resultSet.getString("category_id"));
        category.setName(resultSet.getString("name"));
        return category;
    }
}
