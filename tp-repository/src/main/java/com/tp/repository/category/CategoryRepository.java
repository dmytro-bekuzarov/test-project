package com.tp.repository.category;

import static com.tp.repository.ConnectionHolder.getConnection;

import com.tp.repository.JdbcHelper;
import com.tp.repository.exception.DAOException;
import com.tp.user.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Category repository.
 *
 * @author Dmytro Bekuzarov
 */
@Repository
public class CategoryRepository {

    private static final String FIND_ALL_CATEGORIES = "SELECT * FROM categories";
    private static final String FIND_CATEGORY_IDS = "SELECT category_id FROM categories WHERE category_id IN ";
    private static final String INSERT_CATEGORY = "INSERT INTO categories(category_id, name) VALUES(?,?)";
    private static final String UPDATE_CATEGORY = "UPDATE categories SET name=? WHERE category_id=?";
    private static final String DELETE_CATEGORY_BY_ID = "DELETE FROM categories WHERE category_id = ?";

    @Autowired
    private JdbcHelper jdbcHelper;
    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * Find all categories.
     *
     * @return the list of categories
     */
    public List<Category> findAll() {
        try (PreparedStatement ps = getConnection().prepareStatement(FIND_ALL_CATEGORIES)) {
            return jdbcHelper.selectList(ps, categoryMapper);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Find ids.
     *
     * @param categoryIds the category ids
     * @return the list of category ids
     */
    public List<String> findIds(Collection<String> categoryIds) {
        StringBuilder sql = new StringBuilder(FIND_CATEGORY_IDS);
        sql.append("(");
        Iterator<String> iterator = categoryIds.iterator();
        while (iterator.hasNext()) {
            String categoryId = iterator.next();
            sql.append("'").append(categoryId).append("'");
            if (iterator.hasNext()) {
                sql.append(", ");
            }
        }
        sql.append(")");
        try (PreparedStatement ps = getConnection().prepareStatement(sql.toString())) {
            return jdbcHelper.selectList(ps, String.class);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Insert new category.
     *
     * @param category the category
     */
    public void insert(Category category) {
        try (PreparedStatement ps = getConnection().prepareStatement(INSERT_CATEGORY)) {
            ps.setString(1, category.getId());
            ps.setString(2, category.getName());
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new DAOException(ex);
        }
    }

    /**
     * Update existing category.
     *
     * @param category the category
     * @return {@code true} if category was updated, {@code false} otherwise
     */
    public boolean update(Category category) {
        try (PreparedStatement ps = getConnection().prepareStatement(UPDATE_CATEGORY)) {
            ps.setString(1, category.getName());
            ps.setString(2, category.getId());
            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {
            throw new DAOException(ex);
        }
    }

    /**
     * Delete category by id.
     *
     * @param categoryId the category id
     * @return {@code true} if category was deleted, {@code false} otherwise
     */
    public boolean deleteById(String categoryId) {
        try (PreparedStatement ps = getConnection().prepareStatement(DELETE_CATEGORY_BY_ID)) {
            ps.setString(1, categoryId);
            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {
            throw new DAOException(ex);
        }

    }
}
