package com.tp.repository.category;

import static com.tp.repository.ConnectionHolder.getConnection;

import com.tp.repository.JdbcHelper;
import com.tp.repository.exception.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

/**
 * User category repository.
 *
 * @author Dmytro Bekuzarov
 */
@Repository
public class UserCategoryRepository {

    private static final String FIND_CATEGORY_IDS_BY_USER_ID
        = "SELECT category_id FROM user_categories WHERE user_id=? ";
    private static final String INSERT = "INSERT INTO user_categories(user_id, category_id) VALUES(?,?)";
    private static final String DELETE_BY_USER_ID = "DELETE FROM user_categories WHERE user_id = ?";
    private static final String DELETE_BY_CATEGORY_ID = "DELETE FROM user_categories WHERE category_id = ?";

    @Autowired
    private JdbcHelper jdbcHelper;

    /**
     * Find user category ids by user id.
     *
     * @param userId the user id
     * @return the list of category ids
     */
    public List<String> findCategoryIdsByUserId(String userId) {
        try (PreparedStatement ps = getConnection().prepareStatement(FIND_CATEGORY_IDS_BY_USER_ID)) {
            ps.setString(1, userId);
            return jdbcHelper.selectList(ps, String.class);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Insert user categories.
     *
     * @param categoryIds the category ids
     * @param userId      the user id
     */
    public void insert(Collection<String> categoryIds, String userId) {
        try (PreparedStatement ps = getConnection().prepareStatement(INSERT)) {
            for (String categoryId : categoryIds) {
                ps.setString(1, userId);
                ps.setString(2, categoryId);
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Delete user categories by user id.
     *
     * @param userId the user id
     */
    public void deleteByUserId(String userId) {
        try (PreparedStatement ps = getConnection().prepareStatement(DELETE_BY_USER_ID)) {
            ps.setString(1, userId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new DAOException(ex);
        }
    }

    /**
     * Delete user categories by category id.
     *
     * @param categoryId the category id
     */
    public void deleteByCategoryId(String categoryId) {
        try (PreparedStatement ps = getConnection().prepareStatement(DELETE_BY_CATEGORY_ID)) {
            ps.setString(1, categoryId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new DAOException(ex);
        }
    }
}
