package com.tp.repository;

import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class JdbcHelper {

    public <T> T select(PreparedStatement preparedStatement, Mapper<T> mapper) throws SQLException {
        List<T> list = selectList(preparedStatement, mapper);
        return list.isEmpty() ? null : list.get(0);
    }

    public <T> List<T> selectList(PreparedStatement preparedStatement, Mapper<T> mapper) throws SQLException {
        List<T> result = new ArrayList<>();
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                T record = mapper.map(resultSet);
                result.add(record);
            }
        }
        return result;
    }

    public <T> List<T> selectList(PreparedStatement preparedStatement, Class<T> c) throws SQLException {
        List<T> result = new ArrayList<>();
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                result.add(resultSet.getObject(1, c));
            }
        }
        return result;
    }

    public <T> T selectOne(PreparedStatement preparedStatement, Class<T> c) throws SQLException {
        try (ResultSet resultSet = preparedStatement.executeQuery()) {
            resultSet.next();
            return resultSet.getObject(1, c);
        }
    }


}
