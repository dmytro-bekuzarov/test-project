package com.tp.repository.user;

import com.tp.repository.Mapper;
import com.tp.repository.category.CategoryMapper;
import com.tp.user.Category;
import com.tp.user.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserDetailsMapper implements Mapper<UserDetails> {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public UserDetails map(ResultSet resultSet) throws SQLException {
        UserDetails userDetails = new UserDetails();
        userDetails.setUser(userMapper.map(resultSet));
        addCategory(resultSet, userDetails);
        while (resultSet.next()) {
            if (!userDetails.getUser().getId().equals(resultSet.getString("user_id"))) {
                break;
            }
            addCategory(resultSet, userDetails);
        }
        resultSet.previous();
        return userDetails;
    }

    private void addCategory(ResultSet resultSet, UserDetails userDetails) throws SQLException {
        Category category = categoryMapper.map(resultSet);
        if (category.getId() != null) {
            userDetails.getCategories().add(category);
        }
    }

}
