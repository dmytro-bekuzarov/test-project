package com.tp.repository.user;

import static com.tp.repository.ConnectionHolder.getConnection;

import com.tp.repository.JdbcHelper;
import com.tp.repository.exception.DAOException;
import com.tp.user.User;
import com.tp.user.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

/**
 * User repository.
 *
 * @author Dmytro Bekuzarov
 */
@Repository
public class UserRepository {

    private static final String FIND_ALL_USERS = "SELECT users.*, categories.* FROM users " +
        "LEFT JOIN user_categories ON users.user_id=user_categories.user_id " +
        "LEFT JOIN categories ON categories.category_id=user_categories.category_id";

    private static final String FIND_USER_BY_ID = FIND_ALL_USERS + " WHERE users.user_id=?";
    private static final String COUNT_USER_BY_ID = "SELECT COUNT(*) FROM users WHERE user_id=?";

    private static final String INSERT_USER = "INSERT INTO users (user_id, username, email, password, create_time) " +
        "VALUES (?,?,?,?,?)";
    private static final String UPDATE_USER = "UPDATE users SET username=?, email=?, password=? WHERE user_id=?";
    private static final String DELETE_USER_BY_ID = "DELETE FROM users WHERE user_id=?";

    @Autowired
    private JdbcHelper jdbcHelper;
    @Autowired
    private UserDetailsMapper userDetailsMapper;

    /**
     * Find all user details.
     *
     * @return the list of user details
     */
    public List<UserDetails> findAllUserDetails() {
        try (PreparedStatement ps = getConnection().prepareStatement(FIND_ALL_USERS)) {
            return jdbcHelper.selectList(ps, userDetailsMapper);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Find user details by id.
     *
     * @param userId the user id
     * @return the list of user details
     */
    public UserDetails findUserDetailsById(String userId) {
        try (PreparedStatement ps = getConnection().prepareStatement(FIND_USER_BY_ID)) {
            ps.setString(1, userId);
            return jdbcHelper.select(ps, userDetailsMapper);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Check if user exists.
     *
     * @param userId the user id
     * @return {@code true} if user exists, {@code false} otherwise
     */
    public boolean exists(String userId) {
        try (PreparedStatement ps = getConnection().prepareStatement(COUNT_USER_BY_ID)) {
            ps.setString(1, userId);
            return jdbcHelper.selectOne(ps, Integer.class) != 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Insert new user.
     *
     * @param user the user
     */
    public void insert(User user) {
        try (PreparedStatement ps = getConnection().prepareStatement(INSERT_USER)) {
            ps.setString(1, user.getId());
            ps.setString(2, user.getUsername());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPassword());
            ps.setTimestamp(5, new Timestamp(user.getCreateTime().getTime()));
            ps.executeUpdate();
        } catch (SQLException ex) {
            throw new DAOException(ex);
        }
    }

    /**
     * Update existing user.
     *
     * @param user the user
     * @return {@code true} if user was updated, {@code false} otherwise
     */
    public boolean update(User user) {
        try (PreparedStatement ps = getConnection().prepareStatement(UPDATE_USER)) {
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getEmail());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getId());
            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {
            throw new DAOException(ex);
        }
    }

    /**
     * Delete user by id.
     *
     * @param userId the user id
     * @return {@code true} if user was deleted, {@code false} otherwise
     */
    public boolean deleteById(String userId) {
        try (PreparedStatement ps = getConnection().prepareStatement(DELETE_USER_BY_ID)) {
            ps.setString(1, userId);
            return ps.executeUpdate() != 0;
        } catch (SQLException ex) {
            throw new DAOException(ex);
        }
    }
}
