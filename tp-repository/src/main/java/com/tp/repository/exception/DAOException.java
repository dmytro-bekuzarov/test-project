package com.tp.repository.exception;

public class DAOException extends RuntimeException {

    public DAOException(Exception cause) {
        super(cause);
    }

    public DAOException(String message, Exception cause) {
        super(message, cause);
    }
}
