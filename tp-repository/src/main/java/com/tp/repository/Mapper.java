package com.tp.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Maps result set to object of type {@code T}.
 *
 * @param <T> the type parameter
 */
public interface Mapper<T> {

    /**
     * Map result set.
     *
     * @param resultSet the result set
     * @return the object
     * @throws SQLException the sql exception
     */
    T map(ResultSet resultSet) throws SQLException;

}
