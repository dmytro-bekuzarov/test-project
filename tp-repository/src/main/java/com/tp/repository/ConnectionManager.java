package com.tp.repository;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Manages connections.
 *
 * @author Dmytro Bekuzarov
 */
@Component
@Configuration
@PropertySource(value = "classpath:db.properties")
public class ConnectionManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionManager.class);

    @Value("${datasource.className}")
    private String dataSourceClassName;

    @Value("${db.url}")
    private String databaseUrl;

    @Value("${db.username}")
    private String username;

    @Value("${db.password}")
    private String password;

    @Value("${dataSource.cachePrepStmts}")
    private boolean cachePrepStmts;

    @Value("${dataSource.prepStmtCacheSize}")
    private int prepStmtCacheSize;

    @Value("${dataSource.prepStmtCacheSqlLimit}")
    private int prepStmtCacheSqlLimit;

    @Value("${dataSource.useServerPrepStmts}")
    private boolean useServerPrepStmts;

    @Value("${db.transaction.isolation}")
    private String transactionIsolation;

    private HikariDataSource dataSource;

    /**
     * Get connection.
     *
     * @return the connection
     */
    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            LOGGER.error("Connection wasn't set", e);
            throw new IllegalStateException("Connection wasn't set", e);
        }
    }

    @PostConstruct
    private void init() {
        HikariConfig config = new HikariConfig();
        config.setInitializationFailFast(true);
        config.setDataSourceClassName(dataSourceClassName);
        config.addDataSourceProperty("url", databaseUrl);
        config.addDataSourceProperty("user", username);
        config.addDataSourceProperty("password", password);
        config.addDataSourceProperty("cachePrepStmts", cachePrepStmts);
        config.addDataSourceProperty("prepStmtCacheSize", prepStmtCacheSize);
        config.addDataSourceProperty("prepStmtCacheSqlLimit", prepStmtCacheSqlLimit);
        config.addDataSourceProperty("useServerPrepStmts", useServerPrepStmts);
        config.setTransactionIsolation("TRANSACTION_" + transactionIsolation);
        dataSource = new HikariDataSource(config);
    }

    @PreDestroy
    private void shutdown() {
        dataSource.close();
    }

}
