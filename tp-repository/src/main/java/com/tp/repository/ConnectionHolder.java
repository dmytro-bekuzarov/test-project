package com.tp.repository;

import java.sql.Connection;

/**
 * Holds the connection to the database.
 *
 * @author Dmytro Bekuzarov
 */
public final class ConnectionHolder {

    private static final ThreadLocal<Connection> CONNECTION_HOLDER = new ThreadLocal<>();

    private ConnectionHolder() {
    }

    /**
     * Get connection.
     *
     * @return the connection
     */
    public static Connection getConnection() {
        return CONNECTION_HOLDER.get();
    }

    /**
     * Set connection.
     *
     * @param connection the connection
     */
    public static void set(Connection connection) {
        CONNECTION_HOLDER.set(connection);
    }

    /**
     * Remove connection.
     */
    public static void remove() {
        CONNECTION_HOLDER.remove();
    }
}