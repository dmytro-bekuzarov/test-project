package com.tp.db.changelog

databaseChangeLog {

    include(file: 'changelog-1.groovy', relativeToChangelogFile: true)

}