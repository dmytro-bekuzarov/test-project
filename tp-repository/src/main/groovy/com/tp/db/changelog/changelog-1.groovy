package com.tp.db.changelog


databaseChangeLog {

    changeSet(id: '2017-02-25-00', author: 'Dmytro Bekuzarov <dima95kh@gmail.com>') {
        comment('Create users table')
        createTable(tableName: 'users', remarks: 'Stores basic info about users') {
            column(name: 'user_id', type: 'varchar(64)', remarks: 'User id')
            column(name: 'username', type: 'varchar(16)', remarks: 'Username')
            column(name: 'email', type: 'varchar(255)', remarks: 'User email')
            column(name: 'password', type: 'varchar(128)', remarks: 'Password hash')
            column(name: 'create_time', type: 'timestamp', remarks: 'Creation time')
        }
        rollback {
        }
    }

    changeSet(id: '2017-02-25-01', author: 'Dmytro Bekuzarov <dima95kh@gmail.com>') {
        comment('Add constraints to users table')

        addNotNullConstraint(tableName: 'users', columnName: 'user_id', columnDataType: 'varchar(64)')
        addNotNullConstraint(tableName: 'users', columnName: 'username', columnDataType: 'varchar(16)')
        addNotNullConstraint(tableName: 'users', columnName: 'password', columnDataType: 'varchar(128)')

        addPrimaryKey(tableName: 'users', columnNames: 'user_id', constraintName: 'pk_users')

        rollback {
        }
    }

    changeSet(id: '2017-02-25-02', author: 'Dmytro Bekuzarov <dima95kh@gmail.com>') {
        comment('Create categories table')
        createTable(tableName: 'categories', remarks: 'Stores info about categories') {
            column(name: 'category_id', type: 'varchar(64)', remarks: 'Category id')
            column(name: 'name', type: 'varchar(255)', remarks: 'Category name')
        }
        rollback {
        }
    }

    changeSet(id: '2017-02-25-03', author: 'Dmytro Bekuzarov <dima95kh@gmail.com>') {
        comment('Add constraints to categories table')

        addNotNullConstraint(tableName: 'categories', columnName: 'category_id', columnDataType: 'varchar(64)')
        addNotNullConstraint(tableName: 'categories', columnName: 'name', columnDataType: 'varchar(255)')

        addPrimaryKey(tableName: 'categories', columnNames: 'category_id', constraintName: 'pk_categories')
        rollback {
        }
    }

    changeSet(id: '2017-02-25-04', author: 'Dmytro Bekuzarov <dima95kh@gmail.com>') {
        comment('Create user_categories table')
        createTable(tableName: 'user_categories', remarks: 'Maps categories to users') {
            column(name: 'user_id', type: 'varchar(64)', remarks: 'User id')
            column(name: 'category_id', type: 'varchar(64)', remarks: 'Category id')
        }
        rollback {
        }
    }


    changeSet(id: '2017-02-25-05', author: 'Dmytro Bekuzarov <dima95kh@gmail.com>') {
        comment('Add constraints to user_categories table')

        addNotNullConstraint(tableName: 'user_categories', columnName: 'user_id', columnDataType: 'varchar(64)')
        addNotNullConstraint(tableName: 'user_categories', columnName: 'category_id', columnDataType: 'varchar(64)')

        addPrimaryKey(
                tableName: 'user_categories',
                columnNames: 'user_id, category_id',
                constraintName: 'pk_user_categories'
        )
        addForeignKeyConstraint(
                constraintName: 'fk_user_categories_to_users',
                baseTableName: 'user_categories',
                baseColumnNames: 'user_id',
                referencedTableName: 'users',
                referencedColumnNames: 'user_id',
                onDelete: 'NO ACTION',
                onUpdate: 'NO ACTION'
        )
        addForeignKeyConstraint(
                constraintName: 'fk_user_categories_to_categories',
                baseTableName: 'user_categories',
                baseColumnNames: 'category_id',
                referencedTableName: 'categories',
                referencedColumnNames: 'category_id',
                onDelete: 'NO ACTION',
                onUpdate: 'NO ACTION'
        )
        rollback {
        }
    }

}