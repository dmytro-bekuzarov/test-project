package com.tp.rest.domain.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserDetailsDto extends UserDto {

    private Date createTime;
    private List<CategoryDto> categories = new ArrayList<>();

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public List<CategoryDto> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDto> categories) {
        this.categories = categories;
    }
}
