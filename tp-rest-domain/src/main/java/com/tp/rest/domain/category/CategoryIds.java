package com.tp.rest.domain.category;

import java.util.ArrayList;
import java.util.List;

public class CategoryIds {

    private List<String> categories = new ArrayList<>();

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }
}
