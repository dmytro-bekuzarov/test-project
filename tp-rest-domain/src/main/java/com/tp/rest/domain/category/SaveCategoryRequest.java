package com.tp.rest.domain.category;


public class SaveCategoryRequest {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
