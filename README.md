Test project guide
================================

This guide describes how to install and use test project.
<br>
<br>
<br>

Installation Prerequisites
----------------
In order to use test project, you must have the following items installed on your machine:
* Java 7
* MySQL server (must be up and running)
<br>
* You should also have an empty database (without any tables, views etc.)
<br>
To create a new database, use the following command:
<br>
`CREATE SCHEMA IF NOT EXISTS ``mydb`` DEFAULT CHARACTER SET utf8 ;`
<br>
Note: you can choose any database name


Installation
----------------
* Clone this project to your machine
* Go to `tp-repository/src/main/resources` directory and open `db.properties` file
* Modify `db.url`, `db.username` and `db.password` properties to correspond your database configuration
* Go back to project root directory
* Run the following command `gradlew update` to update database schema
* Run `gradlew bootRun` to run the project
* Project will be available at `http://localhost:8080`

General API info
----------------
* There is almost no validation for request payload
* No authentication


Categories
------------------------

<br>
<b>Get all categories</b>
<br>
<br>
Url: `http://localhost:8080/categories`
<br>
Headers: `Content-Type: application/json`
<br>
Method: GET
<br>
Response example:
```json
[
  {
    "id": "26bd1d67-a8f4-4415-bf1a-0b6beae361ab",
    "name": "Extra category"
  },
  {
    "id": "6ba82867-ccfc-44ec-aa17-85f0e0b015d6",
    "name": "Main category"
  }
]
```

<br>
<b>Add new category</b>
<br>
<br>
Url: `http://localhost:8080/categories`
<br>
Headers: `Content-Type: application/json`
<br>
Method: POST
<br>
Request body:
```json
{
	"name" : "Main category"
}
```
Response example:
```json
{
  "id": "26bd1d67-a8f4-4415-bf1a-0b6beae361ab",
  "name": "Main category"
}
```
<br>

<b>Update existing category</b>
<br>
<br>
Url: `http://localhost:8080/categories/{categoryId}`
<br>
Headers: `Content-Type: application/json`
<br>
Method: PUT
<br>
Request body:
```json
{
	"name" : "Extra category"
}
```
Response example:
```json
{
  "id": "26bd1d67-a8f4-4415-bf1a-0b6beae361ab",
  "name": "Extra category"
}
```
<br>

<b>Delete category</b>
<br>
<br>
Url: `http://localhost:8080/categories/{categoryId}`
<br>
Headers: `Content-Type: application/json`
<br>
Method: DELETE
<br>

Users
------------------------
<br>
<b>Add new user</b>
<br>
<br>
Url: `http://localhost:8080/users`
<br>
Headers: `Content-Type: application/json`
<br>
Method: POST
<br>
Request body:
```json
{
	"username" : "johndoe",
	"email" : "johndoe@gmail.com",
	"password" : "12345"
}
```
Response example:
```json
{
  "userId": "82847b28-d70c-4a96-a7b0-bf34a9598640",
  "username": "johndoe",
  "email": "johndoe@gmail.com",
  "password": "5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5"
}
```
Note: it's a password hash in the response, not an actual password

<br>
<b>Update existing user</b>
<br>
<br>
Url: `http://localhost:8080/users/{userId}`
<br>
Headers: `Content-Type: application/json`
<br>
Method: PUT
<br>
Request body:
```json
{
	"username" : "janedoe",
	"email" : "janedoe@gmail.com",
	"password" : "1234567"
}
```
Response example:
```json
{
  "userId": "82847b28-d70c-4a96-a7b0-bf34a9598640",
  "username": "janedoe",
  "email": "janedoe@gmail.com",
  "password": "8bb0cf6eb9b17d0f7d22b456f121257dc1254e1f01665370476383ea776df414"
}
```
Note: it's a password hash in the response, not an actual password

<br>
<b>Delete user</b>
<br>
<br>
Url: `http://localhost:8080/users/{userId}`
<br>
Headers: `Content-Type: application/json`
<br>
Method: DELETE
<br>

User categories
------------------------
<br>
<b>Add categories to user</b>
<br>
<br>
Note: duplicate categories in the request body are ignored. 
If you try to add the category which already belongs to the user, it will be ignored, too.
<br>
Url: `http://localhost:8080/users/{userId}/categories`
<br>
Headers: `Content-Type: application/json`
<br>
Method: POST
<br>
Request body:
```json
{
	"categories" : ["26bd1d67-a8f4-4415-bf1a-0b6beae361ab"]
}
```

<br>
<b>Replace user categories</b>
<br>
<br>
Note: this endpoint replaces all user's categories with the new ones, specified in the request body.
Duplicate categories in the request body are ignored.
<br>
Url: `http://localhost:8080/users/{userId}/categories`
<br>
Headers: `Content-Type: application/json`
<br>
Method: PUT
<br>
Request body:
```json
{
	"categories" : ["6ba82867-ccfc-44ec-aa17-85f0e0b015d6"]
}
```

<br>
<b>Delete user categories</b>
<br>
<br>
Note: this endpoint deletes all categories that belong to the user
<br>
Url: `http://localhost:8080/users/{userId}/categories`
<br>
Headers: `Content-Type: application/json`
<br>
Method: DELETE
<br>


User details
------------------------
<br>
<b>Get all user with details</b>
<br>
<br>
Url: `http://localhost:8080/users/details`
<br>
Headers: `Content-Type: application/json`
<br>
Method: GET
<br>
Response example:
```json
[
  {
    "userId": "45fc277d-a3ff-47c5-80f8-662972bf5918",
    "username": "janedoe",
    "email": "janedoe@gmail.com",
    "password": "8bb0cf6eb9b17d0f7d22b456f121257dc1254e1f01665370476383ea776df414",
    "createTime": 1488117032000,
    "categories": []
  },
  {
    "userId": "82847b28-d70c-4a96-a7b0-bf34a9598640",
    "username": "janedoe",
    "email": "janedoe@gmail.com",
    "password": "8bb0cf6eb9b17d0f7d22b456f121257dc1254e1f01665370476383ea776df414",
    "createTime": 1488116760000,
    "categories": [
      {
        "id": "6ba82867-ccfc-44ec-aa17-85f0e0b015d6",
        "name": "Main category"
      }
    ]
  }
]
```

<br>
<b>Get user with details by id</b>
<br>
<br>
Url: `http://localhost:8080/users/{userId}/details`
<br>
Headers: `Content-Type: application/json`
<br>
Method: GET
<br>
Response example:
```json
{
  "userId": "82847b28-d70c-4a96-a7b0-bf34a9598640",
  "username": "janedoe",
  "email": "janedoe@gmail.com",
  "password": "8bb0cf6eb9b17d0f7d22b456f121257dc1254e1f01665370476383ea776df414",
  "createTime": 1488116760000,
  "categories": [
    {
      "id": "6ba82867-ccfc-44ec-aa17-85f0e0b015d6",
      "name": "Main category"
    }
  ]
}
```

Running from jar
----------------
You can also run the project from jar file:
* Build the project by running `gradlew build` command from project root directory
* Go to `tp-rest/build/libs`
* Run the following command `java -jar tp-rest-{projectVersion}.jar`
